#!/usr/bin/python3

__version__ = 0.2

'''
A rough script using pytube to download audio only from YouTube.

If downloding by titles, enter the titles as a list (below) in series = [ enter list here ]
add the already downloaded titles to to done = [] list
so they are skipped on a rerun.
Set the downloads dir below

Note: from pytube https://github.com/nficano/pytube/issues/333
FYI if you want to install the latest source directly from github using
pip, type this in your command line:
/usr/bin/pip3 install git+https://github.com/nficano/pytube.git
'''

import pytube
import urllib.request
import urllib.parse
import re
import multiprocessing as mp
import sys
from math import ceil
import requests
import os
import unicodedata
import string
import pathlib
import imp


download_dir = str(pathlib.Path.home()) + '/Downloads/youtube'
pathlib.Path(download_dir).mkdir(parents=True, exist_ok=True)

downloaded = []

### Change to false to ask on each file
download_on_match = True

### These are the possible choices for audio only download from YouTube
audio_stream_type = [
{"itag":141, "mime_type":"audio/mp4", "abr":"256kbps", "acodec":"mp4a.40.2"},
{"itag":172, "mime_type":"audio/webm", "abr":"256kbps", "acodec":"opus"},
{"itag":251, "mime_type":"audio/webm", "abr":"160kbps", "acodec":"opus"},
{"itag":140, "mime_type":"audio/mp4", "abr":"128kbps", "acodec":"mp4a.40.2"}, 
{"itag":171, "mime_type":"audio/webm", "abr":"128kbps", "acodec":"vorbis"},
{"itag":250, "mime_type":"audio/webm", "abr":"70kbps", "acodec":"opus"}, 
{"itag":249, "mime_type":"audio/webm", "abr":"50kbps", "acodec":"opus"},
{"itag":139, "mime_type":"audio/mp4", "abr":"48kbps", "acodec":"mp4a.40.2"}, 
]

audio_itags = []
for st in audio_stream_type:
    audio_itags.append(st["itag"])

### download_from_titles
### add the titles here that are to be downloaded ###

#### Get list data from file
download_list = imp.load_source('download_list', 'download_list.py.txt')
if len(download_list.series) > 0:
    series = download_list.series
else:
    series = ["Mix - Best POP Christmas Songs Playlist 2019 - Merry Christmas 2019 - Popular POP Christmas Songs 2019",
    "Mix - Best POP Christmas Songs Playlist 2019"]

### add the titles here that have already been downloaded ###
done = ["Mix - Best POP Christmas Songs Playlist 2019 - Merry Christmas 2019 - Popular POP Christmas Songs 2019",]

q_yes = lambda q: input(q).lower().strip()[0] == "y"
valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
char_limit = 255
title_list = []
CHUNK_SIZE = 3 * 2**20  # bytes

def clean_filename(filename, whitelist=valid_filename_chars, replace='/'):
    '''from https://gist.github.com/wassname/1393c4a57cfcbf03641dbc31886123b8'''
    # replace spaces - changed to replace / with -
    for r in replace:
        filename = filename.replace(r,'-')
    
    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore').decode()
    
    # keep only whitelisted chars
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename)>char_limit:
        print("Warning, filename truncated because it was over {}. Filenames may no longer be unique".format(char_limit))
        
    return cleaned_filename[:char_limit] 

def download_video(stream, itag, filename):
    '''from code in https://github.com/nficano/pytube/issues/180'''

    url = stream.url
    filesize = stream.filesize

    ranges = [[url, i * CHUNK_SIZE, (i+1) * CHUNK_SIZE - 1] for i in range(ceil(filesize / CHUNK_SIZE))]
    ranges[-1][2] = None  # Last range must be to the end of file, so it will be marked as None.

    pool = mp.Pool(min(len(ranges), 64))
    chunks = [0 for _ in ranges]
    
    for i, chunk_tuple in enumerate(pool.imap_unordered(download_chunk, enumerate(ranges)), 1):
        idx, chunk = chunk_tuple
        chunks[idx] = chunk
        sys.stderr.write('\rDownload completion: {0:%}'.format(i/len(ranges)))
        
    print()
    with open(filename, 'wb') as outfile:
        for chunk in chunks:
            outfile.write(chunk)

def download_chunk(args):
    idx, args = args
    url, start, finish = args
    range_string = '{}-'.format(start)

    if finish is not None:
        range_string += str(finish)

    response = requests.get(url, headers={'Range': 'bytes=' + range_string})
    return idx, response.content
    
def get_stream(yt_obj):
    found = False
    yt_streams = yt.streams.filter(only_audio=True, ).all()
    for tag  in audio_itags:
        if found:
            break
        for i, st in enumerate(yt_streams):
            if tag == int(st.itag):
                found = True
                form, ext = st.mime_type.split("/")
                print ("Downloading: %s" % yt.title)
                print ("Using format %s type %s at %s" % (form, ext, st.abr))
                download_video(yt_streams[i], tag, os.path.join(download_dir, clean_filename(yt.title)+'.'+ext))
                break


#"Do you want to automatically download? (y/n)"):

print("Choose one of the following options:")
print ("\t1. Dowload using YouTube URL")
print ("\t2. Download using YouTube playlist")
print ("\t3. Download using YouTube titles")
ans = int(input("Enter a number: ").lower().strip())

if ans == 1:
    ### download_from_url
    url = str(input("Enter the URL: "))
    yt = pytube.YouTube(url)

    if not download_on_match:
        if not q_yes("Do wish to download? \t{} (y/n)".format(yt.title)):
            sys.exit()
    get_stream(yt)

elif ans == 2:
    ### download_from_playlist
    playlist = str(input("Enter the playlist URL: "))
    pl = pytube.Playlist(playlist)
    pl.populate_video_urls()
    print ("{} files...".format(len(pl.video_urls)))
    if len(pl.video_urls) >0 :
        for i, url in enumerate(pl.video_urls):
            yt = pytube.YouTube(url)
            print("{}. {}".format(i+1, yt.title))
            if not download_on_match:
                if not q_yes("Do wish to download? (y/n)"):
                    continue
            get_stream(yt)

elif ans == 3:
    ### download_from_titles
    for title in series:
        if title not in done:
            print ("Searching for {}...".format(title))
            query_string = urllib.parse.urlencode({"search_query" : "intitle:"+"'"+title+"'" })        
            html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
            search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content.read().decode())
            
            for i, result in enumerate(search_results):
                search_url = "http://www.youtube.com/watch?v=" + search_results[i]
                yt = pytube.YouTube(search_url)

                if yt.title in downloaded:
                    continue

                if yt.title in series and yt.title not in done and yt.title not in title_list:            
                    print ("Found {}...".format(yt.title))
                    if not download_on_match:
                        if not q_yes("Do wish to download ***{}*** (y/n)".format(yt.title)):
                            print ("\tSkipping {}".format(yt.title))
                            break
                    title_list.append(yt.title)
                    get_stream(yt)
                    downloaded.append(yt.title)
                    continue
                else:
                    if not download_on_match:
                        if not q_yes("Do wish to download ***{}*** (y/n)".format(yt.title)):
                            print ("\tSkipping {}".format(yt.title))
                            continue
                    get_stream(yt)
                    downloaded.append(yt.title)

else:
    print ("Choice {} is invalid".format(ans))
    sys.exit()
    
    



  
